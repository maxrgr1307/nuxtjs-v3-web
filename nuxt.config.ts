import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  meta: {
    meta: [
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    // link: [
    //   { href: 'https://rsms.me/inter/inter.css' },
    //   { rel: 'icon', type: 'image/svg+xml', href: '/favicon.svg' },
    //   { rel: 'alternate icon', href: '/favicon.ico' }
    // ],
    // script: [{ src: '/assets/js/vite-amplify-fix.js' }]
  },
  buildModules: [
    // '@vueuse/nuxt',
    // '@pinia/nuxt',
  ],
    build: {
        postcss: {
          postcssOptions: {
            plugins: {
              tailwindcss: {},
              autoprefixer: {},
            },
          },
        },
    },
    css: [
        "~/assets/css/tailwind.css"
    ],
})
